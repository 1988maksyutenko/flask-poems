FROM python:3.10-alpine

RUN apk update \
    apk add bash \
    apk add python3 \
    apk add python3-dev \
    apk add py3-pip

RUN python -m pip install --upgrade pip

RUN mkdir /poems

COPY . poems/

WORKDIR /poems

RUN apk add postgresql-dev gcc musl-dev build-base

RUN pip install -r app/settings/dev/requirements.txt
RUN pip install --upgrade 'sentry-sdk[flask]'


CMD [ "python", "run.py" ]