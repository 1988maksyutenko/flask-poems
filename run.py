from app.app import create_app

app = create_app('dev')
app.run(host='0.0.0.0', port=8080)
