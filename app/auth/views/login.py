from flask import request, jsonify

from flask_restful import Resource

from flask_jwt_extended import create_access_token, set_access_cookies

from marshmallow import ValidationError

from app.app import api
from app.auth.domain import UserManager
from app.core.exceptions.models import UserDoesNotExisits


class Login(Resource):
    def post(self):
        data = request.get_json()
        manager = UserManager()
        try:
            manager.user_login(data)
            domain = request.args.get("domain")
            token = create_access_token(identity=data['email'])
            response = jsonify({'login': 'success'})
            set_access_cookies(response, token, domain)
            return response
        except UserDoesNotExisits as err:
            return {'error': err.message}, 401
        except ValidationError as err:
            return {'error': err.messages}, 401



api.add_resource(Login, '/login')
