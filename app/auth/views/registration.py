from flask import request
from flask_restful import Resource
from marshmallow import ValidationError

from app.app import api
from app.auth.domain import UserManager
from app.core.exceptions.models import UserAlreadyExisits

class Registration(Resource):
    def post(self):
        data = request.get_json()
        manager = UserManager()

        try:
            manager.create_new_user(data)
        except ValidationError as err:
            return {'error': err.messages}, 400
        except UserAlreadyExisits as err:
            return {'error': err.message}, 400
        return {'status': 'success'}


api.add_resource(Registration, '/reg')
