from marshmallow import ValidationError

from app.core.models import Users
from app.core.serializers import UsersSchema
from app.core.exceptions.models import UserAlreadyExisits, UserDoesNotExisits, ObjectDoesNotExists


class UserManager:
    def create_new_user(self, data):
        try:
            serialized_data = UsersSchema().load(data)
            return Users(
                password=serialized_data['password'],
                first_name=serialized_data['first_name'],
                last_name=serialized_data['last_name'],
                email=serialized_data['email']
            ).create()
        except ValidationError as err:
            raise ValidationError(err.messages)
        except ObjectDoesNotExists:
            raise UserAlreadyExisits()

    def user_login(self, data):
        try:
            serialized_data = UsersSchema().load(data)
            Users.read(email=serialized_data['email'])
            return True
        except ValidationError as err:
            raise ValidationError(err.messages)
        except ObjectDoesNotExists:
            raise UserDoesNotExisits()