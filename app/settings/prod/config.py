import os
from datetime import timedelta

from app.settings.common_config import *  # noqa


ENV = 'prod'

DEBUG = False

SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI', None)

JWT_COOKIE_SECURE = True
JWT_TOKEN_LOCATION = ["cookies"]
JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=1)

