import secrets

ENV = None

SQLALCHEMY_TRACK_MODIFICATIONS = False

JWT_SECRET_KEY = secrets.token_urlsafe(64)
