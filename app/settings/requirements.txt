Flask==2.0
psycopg2==2.9.1
sqlalchemy==1.4.25
Flask-Migrate==3.1.0
flask-restful==0.3.9
flask-jwt-extended==4.3.1
marshmallow==3.14.0
pytest==6.2.5