import os
from datetime import timedelta

from app.settings.common_config import *  # noqa


ENV = 'test'

DEBUG = True

SQLALCHEMY_DATABASE_URI = 'postgresql://user:password@test_postgres:5433/test_poems'

JWT_COOKIE_SECURE = False
JWT_TOKEN_LOCATION = ["cookies"]
JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=1)
JWT_COOKIE_CSRF_PROTECT = True
