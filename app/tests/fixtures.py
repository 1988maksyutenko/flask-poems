import pytest


@pytest.fixture
def create_user():
    return {
        'first_name': 'fname',
        'last_name': 'lname',
        'email': 'qw3@mail.com',
        'password': 'gasd4'
    }


@pytest.fixture
def login_user():
    return {
        'email': 'qw3@mail.com',
        'password': 'gasd4'
    }
