def get_csrf_token(headers_list):
    for item in headers_list:
        if 'csrf_access_token' in item:
            values = item.split('=')
            token = values[1].split(';')[0]
            return token
