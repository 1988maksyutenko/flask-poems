import pytest

from app.app import create_app, db, jwt


@pytest.fixture
def app():
    import app.core.models

    import app.auth.views
    import app.poems.views

    app = create_app('test')
    app.config.from_object('app.settings.test.config')
    db.init_app(app)
    jwt.init_app(app)
    with app.test_client() as client:
        with app.app_context():   
            db.create_all()
            yield client
            db.session.remove()
            db.drop_all()
