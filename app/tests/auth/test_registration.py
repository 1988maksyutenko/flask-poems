from app.tests.fixtures import create_user

def test_registration_success(app, create_user):
    response = app.post(
        '/reg',
        json=create_user
    )

    assert response.get_json() == {'status': 'success'}
    assert response.status == '200 OK'


def test_registration_error(app):
    response = app.post(
        '/reg',
        json={
            'first_nam': 'name',
        }
    )

    assert response.get_json() == {'error': {'first_nam': ['Unknown field.']}}
    assert response.status == '400 BAD REQUEST'


def test_registration_user_already_exists(app, create_user):
    app.post(
        '/reg',
        json=create_user
    )
    response = app.post(
        '/reg',
        json=create_user
    )

    assert response.status == '400 BAD REQUEST'
    assert response.get_json() == {'error': 'User already exists!'}
