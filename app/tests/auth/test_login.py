from app.core.models import Users

email = '123@mail.com'
password = '1234qwre'
user = Users(
    first_name='testname',
    last_name='lastname',
    password=password,
    email=email
)

def test_login_success(app):
    user.create()

    response = app.post(
        '/login',
        json={
            'email': email,
            'password': password
        }
    )

    assert response.status == '200 OK'
    assert response.get_json().get('login', False)
    assert type(response.get_json().get('login', None)) == str


def test_login_user_does_not_exists(app):
    user.create()

    response = app.post(
        '/login',
        json={
            'email': email,
            'password': password
        }
    )

    assert response.status == '401 UNAUTHORIZED'
    assert response.get_json() == {'error': 'User does not exists!'}


def test_login_user_validation_error(app):
    user.create()

    response = app.post(
        '/login',
        json={
            'mail': email,
            'password': password
        }
    )

    assert response.status == '401 UNAUTHORIZED'
    assert response.get_json() == {'error': {'mail': ['Unknown field.']}}