import pytest
from marshmallow import ValidationError

from app.auth.domain import UserManager
from app.core.models import Users
from app.core.exceptions.models import UserAlreadyExisits, UserDoesNotExisits
from app.tests.fixtures import create_user, login_user


def test_manager_new_user_creation(app, create_user):
    manager = UserManager()
    new_user = manager.create_new_user(create_user)

    assert Users.read(id=1)[0].id == new_user.id


def test_manager_user_does_not_exists(app):
    manager = UserManager()

    with pytest.raises(UserDoesNotExisits):
        manager.user_login({'email': 'test@mail.com', 'password': 'test'})



def test_manager_new_user_creation_validation_error(app):
    test_user = {
        'email': 'mail@mail.com',
        'name': 'some'
    }
    manager = UserManager()
    with pytest.raises(ValidationError):
        manager.create_new_user(test_user)


def test_manager_new_user_creation_exists_error(app, create_user):
    manager = UserManager()
    manager.create_new_user(create_user)
    with pytest.raises(UserAlreadyExisits):
        manager.create_new_user(create_user)


def test_user_login(app, create_user, login_user):
    manager = UserManager()
    manager.create_new_user(create_user)

    assert manager.user_login(login_user) == True


def test_user_login_validation_error(app):
    test_user = {
        'mail': 'test@mail.com'
    }
    manager = UserManager()
    with pytest.raises(ValidationError):
        manager.user_login(test_user)


def test_user_login_does_not_exist_error(app, login_user):
    manager = UserManager()

    login_user['email'] = 'someother@mail.com'
    with pytest.raises(UserDoesNotExisits):
        manager.user_login(login_user)