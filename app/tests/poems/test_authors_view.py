from app.tests.utils import get_csrf_token
from app.tests.fixtures import create_user, login_user
from app.tests.poems.fixtures import create_author, create_author2


def test_create_author_view_success(app, create_user, login_user, create_author):
    app.post('/reg', json=create_user)
    response = app.post('/login', json=login_user)

    token = get_csrf_token(response.headers.getlist('Set-Cookie'))

    response = app.post(
        '/authors',
        json=create_author,
        headers={'X-CSRF-TOKEN': token}
    )

    assert response.status == '200 OK'
    assert response.get_json()['first_name'] == create_author['first_name']
    assert response.get_json()['last_name'] == create_author['last_name']


def test_create_author_view_jwt_required(app, create_author):
    response = app.post('/authors', json=create_author)

    assert response.status == '401 UNAUTHORIZED'


def test_create_author_validation_error(app, create_user, login_user):
    app.post('/reg', json=create_user)
    response = app.post('/login', json=login_user)

    token = get_csrf_token(response.headers.getlist('Set-Cookie'))

    response = app.post(
        '/authors',
        json={'name': 'some', 'last_name': 'some'}, 
        headers={'X-CSRF-TOKEN': token}
    )

    assert response.get_json()['error'] == {'name': ['Unknown field.']}


def test_create_author_view_already_exists_error(app, create_author, login_user, create_user):
    app.post('/reg', json=create_user)
    response = app.post('/login', json=login_user)

    token = get_csrf_token(response.headers.getlist('Set-Cookie'))

    response = app.post(
        '/authors',
        json=create_author,
        headers={'X-CSRF-TOKEN': token}
    )

    response = app.post(
        '/authors',
        json=create_author,
        headers={'X-CSRF-TOKEN': token}
    )
    assert \
    response.get_json()['error'] == \
    f'{create_author["first_name"]} {create_author["last_name"]} already exists.'


def test_authors_view_list_retrieving_success(
    app, 
    create_author, 
    create_author2,
    login_user, 
    create_user):
    app.post('/reg', json=create_user)
    response = app.post('/login', json=login_user)

    token = get_csrf_token(response.headers.getlist('Set-Cookie'))

    app.post('/authors', json=create_author, headers={'X-CSRF-TOKEN': token})
    app.post('/authors', json=create_author2, headers={'X-CSRF-TOKEN': token})

    create_author.setdefault('id', 1)
    create_author2.setdefault('id', 2)

    response = app.get('/authors', headers={'X-CSRF-TOKEN': token})

    assert response.status == '200 OK'
    assert response.get_json() == [create_author, create_author2] 


def test_authors_view_author_retrieving_404(
        app, 
        create_author, 
        login_user, 
        create_user
    ):
    app.post('/reg', json=create_user)
    response = app.post('/login', json=login_user)
    token = get_csrf_token(response.headers.getlist('Set-Cookie'))
    app.post('/authors', json=create_author, headers={'X-CSRF-TOKEN': token})
    create_author.setdefault('id', 1)

    response = app.get('/authors/2', headers={'X-CSRF-TOKEN': token})

    assert response.status == '404 NOT FOUND'



def test_authors_view_author_success(
        app, 
        create_author, 
        login_user, 
        create_user
    ):
    app.post('/reg', json=create_user)
    response = app.post('/login', json=login_user)
    token = get_csrf_token(response.headers.getlist('Set-Cookie'))
    app.post('/authors', json=create_author, headers={'X-CSRF-TOKEN': token})
    create_author.setdefault('id', 1)

    response = app.get('/authors/1', headers={'X-CSRF-TOKEN': token})

    assert response.status == '200 OK'
    assert response.get_json() == create_author