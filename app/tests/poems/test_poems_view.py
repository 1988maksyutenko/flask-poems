from app.tests.fixtures import create_user, login_user
from app.tests.poems.fixtures import create_author, poem, poems
from app.tests.utils import get_csrf_token

def test_poems_view_jwt_falied(app, poem):
    response = app.post('/poems', json=poem)

    assert response.status == '401 UNAUTHORIZED'


def test_poems_view_creation_success(
        app,
        create_user,
        login_user,
        create_author,
        poem
    ):
    app.post('/reg', json=create_user)
    response = app.post('/login', json=login_user)
    token = get_csrf_token(response.headers.getlist('set-cookie'))
    app.post('/authors', json=create_author, headers={'x-csrf-token': token})

    response = app.post('/poems', json=poem, headers={'x-csrf-token': token})

    assert response.status == '200 OK'
    assert response.get_json()['name'] == poem['name']


def test_poems_view_validation_error(
        app,
        create_user,
        login_user,
        create_author
    ):
    app.post('/reg', json=create_user)
    response = app.post('/login', json=login_user)
    token = get_csrf_token(response.headers.getlist('set-cookie'))
    app.post('/authors', json=create_author, headers={'x-csrf-token': token})

    response = app.post(
        '/poems',
        json={'nam': 'test'},
        headers={'x-csrf-token': token}
    )

    assert response.get_json()['error'] == {'nam': ['Unknown field.']}


def test_poems_view_creation_already_exists_error(
        app,
        create_user,
        login_user,
        create_author,
        poem
    ):
    app.post('/reg', json=create_user)
    response = app.post('/login', json=login_user)
    token = get_csrf_token(response.headers.getlist('set-cookie'))
    app.post('/authors', json=create_author, headers={'x-csrf-token': token})

    response = app.post('/poems', json=poem, headers={'x-csrf-token': token})
    response = app.post('/poems', json=poem, headers={'x-csrf-token': token})

    assert response.get_json()['error'] == 'Test poem name. poem already exists.'


def test_poems_view_get_multiple_poems(
        app,
        create_user,
        login_user,
        create_author,
        poems
    ):
    app.post('/reg', json=create_user)
    response = app.post('/login', json=login_user)
    token = get_csrf_token(response.headers.getlist('set-cookie'))
    app.post('/authors', json=create_author, headers={'x-csrf-token': token})
    for poem in poems:
        app.post('/poems', json=poem, headers={'x-csrf-token': token})
    response = app.get('/poems', json={}, headers={'x-csrf-token': token}) 

    id_value = 1
    for poem in poems:
        poem.setdefault('id', id_value)
        id_value += 1
    assert response.status == '200 OK'
    assert response.get_json() == poems
