import pytest


@pytest.fixture
def create_author():
    return {
        'first_name': 'testname', 
        'last_name': 'testlastname'
    }

@pytest.fixture
def create_author2():
    return {
        'first_name': 'Testname', 
        'last_name': 'Testlastname'
    }
    
@pytest.fixture
def poem():
    return {
        'name': 'Test poem name.',
        'text': 'Some poem text.',
        'author_id': 1
    }

@pytest.fixture
def poems():
    return [
        {
            'name': f'name{i}',
            'text': f'text{i}',
            'author_id': 1
        }
        for i in range(5)
    ]
