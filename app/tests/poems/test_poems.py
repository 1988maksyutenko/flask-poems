import pytest

from marshmallow import ValidationError

from app.poems.domain import PoemsManager, AuthorManager
from app.poems.models import Poems
from app.poems.exceptions.models import PoemAlreadyExists

from app.tests.fixtures import create_user, login_user
from app.tests.poems.fixtures import poem, poems, create_author


def test_poem_cration(app, poem, create_author):
    author = AuthorManager().create_new_author(create_author)

    new_poem = poem
    manager = PoemsManager()
    manager.create_new_poem(new_poem)

    created_poem = Poems.read(id=1)

    assert created_poem[0].name == new_poem['name']
    assert created_poem[0].text == new_poem['text']
    assert created_poem[0].author_id == author.id


def test_poem_already_exists_error(app, poem, create_author):
    AuthorManager().create_new_author(create_author)

    manager = PoemsManager()
    manager.create_new_poem(poem)

    with pytest.raises(PoemAlreadyExists):
        manager.create_new_poem(poem)


def test_poem_validation_error(app):
    manager = PoemsManager()

    with pytest.raises(ValidationError):
        manager.create_new_poem({'name': 'test', 'ext': 'tet'})


def test_poems_get_poems(app, poems, create_author):
    AuthorManager().create_new_author(create_author)
    
    manager = PoemsManager()
    for poem in poems:
        manager.create_new_poem(poem)

    id_value = 1
    for poem in poems:
        poem.setdefault('id', id_value)
        id_value += 1
    
    assert manager.get_poems() == poems
