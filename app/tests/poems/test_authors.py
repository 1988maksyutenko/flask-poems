from types import CellType
import pytest

from marshmallow import ValidationError

from app.poems.domain import AuthorManager
from app.poems.models import Authors
from app.poems.exceptions.models import AuthorAlreadyExists, AuthorDoesNotExists
from app.tests.poems.fixtures import create_author, create_author2

def test_author_creation(app, create_author):
    manager = AuthorManager()
    manager.create_new_author(create_author)

    Authors.read(id=1)[0].firs_name = create_author['first_name']


def test_author_creation_already_exists(app, create_author):
    manager = AuthorManager()
    manager.create_new_author(create_author)
    with pytest.raises(AuthorAlreadyExists):
        manager.create_new_author(create_author)


def test_author_creation_validation_error(app):
    manager = AuthorManager()
    with pytest.raises(ValidationError):
        manager.create_new_author({'ame': 'test'})


def test_author_list_retrieving(app, create_author, create_author2):
    manager = AuthorManager()
    manager.create_new_author(create_author)
    manager.create_new_author(create_author2)
    create_author.setdefault('id', 1)
    create_author2['id'] = 2

    authors_list = manager.authors_list()

    assert authors_list == [create_author, create_author2]


def test_author_retrieving(app, create_author, create_author2):
    manager = AuthorManager()
    manager.create_new_author(create_author)
    manager.create_new_author(create_author2)
    create_author.setdefault('id', 1)
    create_author2.setdefault('id', 2)

    author1 = manager.get_author(1)
    author2 = manager.get_author(2)

    assert author1['first_name'] == create_author['first_name']
    assert author1['last_name'] == create_author['last_name']
    assert author2['first_name'] == create_author2['first_name']
    assert author2['last_name'] == create_author2['last_name']


def test_author_retrieving_not_exists_error(app):
    manager = AuthorManager()

    with pytest.raises(AuthorDoesNotExists):
        manager.get_author(1)
