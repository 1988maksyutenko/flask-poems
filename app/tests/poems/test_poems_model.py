from app.poems.models import Poems
from app.poems.domain import AuthorManager
from app.tests.poems.fixtures import poem, create_author


def test_poem_str_method(app, poem, create_author):
    AuthorManager().create_new_author(create_author)
    created_poem = Poems(**poem).create()
    assert str(created_poem) == f"{poem['name']}"


def test_poem_repr_method(app, poem, create_author):
    AuthorManager().create_new_author(create_author)
    created_poem = Poems(**poem).create()
    assert repr(created_poem) == f"{poem['name']}"
