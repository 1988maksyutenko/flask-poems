from app.poems.models import Authors

from app.tests.poems.fixtures import create_author


def test_str_method(app, create_author):
    author = Authors(**create_author).create()
    assert str(author) == f"{create_author['first_name']} {create_author['last_name']}"


def test_repr_metshod(app, create_author):
    author = Authors(**create_author).create()
    assert repr(author) == f"{create_author['first_name']} {create_author['last_name']}"
