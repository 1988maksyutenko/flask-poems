import pytest

from app.tests.fixtures import create_user
from app.core.models import Users
from app.core.exceptions.models import (
    UserAlreadyExisits, ColumnDoesNotExisits, UserDoesNotExisits
)


def test_user_creation(app, create_user):
    test_user = Users(**create_user)
    created_user = test_user.create()
    assert created_user == test_user

def test_user_cration_error(app, create_user):
    test_user = Users(**create_user)
    test_user1 = Users(**create_user)
    with pytest.raises(UserAlreadyExisits) as excinfo:
        test_user.create()
        test_user1.create()
    assert str(excinfo.value) == 'User already exists!'

def test_read_all_users(app):
    new_users = [
        Users(
            first_name=f'Name{i}',
            last_name=f'LastName{i}',
            password='123321',
            email=f'123{i}@mail.com'
        ).create()
        for i in range(5)
    ]
    readed_users = Users.read()
    assert readed_users == new_users

def test_read_user_by_id(app, create_user):
    new_user = Users(**create_user).create()
    assert new_user.id == Users.read(id=1)[0].id

def test_read_user_by_id_fail(app, create_user):
    Users(**create_user).create()

    with pytest.raises(UserDoesNotExisits) as excinfo:
        Users.read(id=2)[0].id
    assert str(excinfo.value) == 'User does not exists!'

def test_read_user_by_name(app, create_user):
    new_user = Users(**create_user).create()
    assert new_user.id == Users.read(first_name=create_user['first_name'])[0].id

def test_read_user_by_name_and_email(app, create_user):
    new_user = Users(**create_user).create()
    assert new_user.id == Users.read(first_name=create_user['first_name'], email=create_user['email'])[0].id

def test_column_name_error(app, create_user):
    Users(**create_user).create()
    with pytest.raises(ColumnDoesNotExisits) as excinfo:
        Users.read(irst_name='Name123')[0].id
    assert str(excinfo.value) == 'Column with that name does not exists!'


def test_update_user_name(app, create_user):
    test_user = Users(**create_user)
    new_name = 'New name'
    test_user.create()
    test_user.update(first_name=new_name)

    assert Users.read(id=1)[0].first_name == new_name


def test_update_user_name_and_email(app, create_user):
    test_user = Users(**create_user)
    new_name = 'New name'
    new_email = '321@mail.com'
    test_user.create()
    test_user.update(first_name=new_name, email=new_email)

    assert Users.read(id=1)[0].first_name == new_name
    assert Users.read(id=1)[0].email == new_email


def test_update_user_return_link(app, create_user):
    test_user = Users(**create_user)
    new_name = 'New name'
    new_email = '321@mail.com'
    test_user.create()
    user = test_user.update(first_name=new_name, email=new_email)

    assert user is test_user


def test_update_raise_error(app, create_user):
    test_user = Users(**create_user)
    new_name = 'New name'
    test_user.create()

    with pytest.raises(ColumnDoesNotExisits):
        test_user.update(firstname=new_name)


def test_user_destroy(app, create_user):
    test_user = Users(**create_user)
    test_user.create()
    test_user.delete()

    with pytest.raises(UserDoesNotExisits):
        Users.read(id=1)
    