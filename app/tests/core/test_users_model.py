from app.core.models import Users
from app.tests.fixtures import create_user


def test_users_str_method(app, create_user):
    user = Users(**create_user).create()
    assert str(user) == f"{1} {create_user['first_name']} {create_user['last_name']}"


def test_users_repr_method(app, create_user):
    user = Users(**create_user).create()
    assert repr(user) == f"{1} {create_user['first_name']} {create_user['last_name']}"
