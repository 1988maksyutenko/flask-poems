from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Api
from flask_jwt_extended import JWTManager

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration


__all__ = ['db', 'migrate', 'api', 'create_app', 'jwt']

db = SQLAlchemy()
migrate = Migrate()
api = Api()
jwt = JWTManager()


def create_app(config: str) -> Flask:
    import app.core.models
    import app.poems.models

    import app.auth.views
    import app.poems.views

    app = Flask(__name__)
    app.config.from_object('app.settings.dev.config')
    db.init_app(app)
    migrate.init_app(app, db)
    api.init_app(app)
    jwt.init_app(app)
    sentry_sdk.init(
        dsn="https://f78c0d0207e6481c8044b215b14a1b72@o1083630.ingest.sentry.io/6093401",
        integrations=[FlaskIntegration()],
        traces_sample_rate=1.0
    )

    return app
