from sqlalchemy.exc import IntegrityError

from app.app import db
from app.core.exceptions.models import (
    ColumnDoesNotExisits, ObjectDoesNotExists
)


class CRUDMixin:
    def create(self):
        try:
            db.session.add(self)
            db.session.commit()
        except IntegrityError:
            raise ObjectDoesNotExists
        return self

    @classmethod
    def read(cls, **kwargs):
        if not kwargs:
            return cls.query.all()
        else:
            if len(kwargs) == 1 and kwargs.get('id', None):
                obj = cls.query.get(kwargs['id'])
                if obj is None:
                    raise ObjectDoesNotExists
                return [obj]
            query = db.session.query(cls)
            try:
                for key, value in kwargs.items():
                    query = query.filter(getattr(cls, key)==value)
                if len(query.all()) == 0:
                    raise ObjectDoesNotExists
                return query.all()
            except AttributeError:
                raise ColumnDoesNotExisits

    def update(self, **kwargs):
            for key, value in kwargs.items():
                if hasattr(self, key):
                    setattr(self, key, value)
                else:
                    raise ColumnDoesNotExisits
            return self

    def delete(self):
        db.session.delete(self)
        db.session.commit()
