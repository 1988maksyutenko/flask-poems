from datetime import datetime

from app.app import db
from app.core.model_mixins import CRUDMixin
from app.core.exceptions.models import (
    ObjectDoesNotExists, UserDoesNotExisits, UserAlreadyExisits
)


__all__ = ['Users']


class Users(CRUDMixin, db.Model):
    """Contains data about users."""

    id = db.Column(db.Integer, primary_key=True)
    password = db.Column(db.String(32), nullable=False, unique=False)
    created_at = db.Column(db.DateTime(), default=datetime.now)
    first_name = db.Column(db.String(32), nullable=True, unique=False)
    last_name = db.Column(db.String(32), nullable=True, unique=False)
    email = db.Column(db.String(120), nullable=False, unique=True)

    def __str__(self) -> str:
        return f'{self.id} {self.first_name} {self.last_name}'

    def __repr__(self) -> str:
        return self.__str__()

    def create(self):
        try:
            return super().create()
        except ObjectDoesNotExists:
            raise UserAlreadyExisits

    @classmethod
    def read(cls, **kwargs):
        try:
            return super().read(**kwargs)
        except ObjectDoesNotExists:
            raise UserDoesNotExisits
