from marshmallow import Schema, fields


__all__ = ('UsersSchema',)


class UsersSchema(Schema):
    id = fields.Int()
    password = fields.Str()
    created_at = fields.DateTime()
    first_name = fields.Str()
    last_name = fields.Str()
    email = fields.Email()
