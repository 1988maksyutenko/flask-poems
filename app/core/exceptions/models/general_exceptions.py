class ObjectDoesNotExists(Exception):
    def __init__(self) -> None:
        self.message =  'Object does not exists!'
        super().__init__(self.message)


class ColumnDoesNotExisits(Exception):
    def __init__(self) -> None:
        self.message =  'Column with that name does not exists!'
        super().__init__(self.message)
