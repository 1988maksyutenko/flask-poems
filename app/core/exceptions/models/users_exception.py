class UserAlreadyExisits(Exception):
    def __init__(self) -> None:
        self.message =  'User already exists!'
        super().__init__(self.message)


class UserDoesNotExisits(Exception):
    def __init__(self) -> None:
        self.message =  'User does not exists!'
        super().__init__(self.message)
