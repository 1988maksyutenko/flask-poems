from sqlalchemy.orm import backref
from sqlalchemy.schema import UniqueConstraint

from app.app import db
from app.core.model_mixins import CRUDMixin
from app.poems.models.poems import Poems


class Authors(CRUDMixin, db.Model):
    """Contain information about authrs of the poem."""

    __table_args__ = (
        UniqueConstraint('first_name', 'last_name', name='identity'),
    )

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64), nullable=False)
    last_name = db.Column(db.String(64), nullable=False)
    poems = db.relationship(Poems, backref='poems', lazy=True)

    def __str__(self) -> str:
        return self.first_name + ' ' + self.last_name

    def __repr__(self) -> str:
        return self.__str__()