from sqlalchemy.orm import joinedload

from app.app import db

from app.core.model_mixins import CRUDMixin
from app.core.exceptions.models import ObjectDoesNotExists
from app.poems.exceptions.models import PoemAlreadyExists


class Poems(CRUDMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), unique=True)
    text = db.Column(db.Text(), nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('authors.id'), nullable=False)

    def __str__(self) -> str:
        return f'{self.name}'

    def __repr__(self) -> str:
        return self.__str__()
