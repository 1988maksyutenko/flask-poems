class  PoemAlreadyExists(Exception):
    def __init__(self, name):
        self.messages = f'{name} poem already exists.'
        super().__init__(self)
