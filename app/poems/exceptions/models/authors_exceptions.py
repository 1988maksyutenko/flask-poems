class AuthorAlreadyExists(Exception):
    def __init__(self, name):
        self.messages = f'{name} already exists.'
        super().__init__(self)


class AuthorDoesNotExists(Exception):
    def __init__(self):
        self.messages = 'Author does not exists.'
        super().__init__(self)
