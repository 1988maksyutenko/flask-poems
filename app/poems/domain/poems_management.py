from marshmallow.exceptions import ValidationError

from app.poems.models import Poems
from app.poems.serializers import PoemsShema
from app.poems.exceptions.models import PoemAlreadyExists
from app.core.exceptions.models import ObjectDoesNotExists


class PoemsManager:

    def create_new_poem(self, new_poem):
        serialized_data = PoemsShema().load(new_poem)
        try:
            return Poems(
                name=serialized_data['name'],
                text=serialized_data['text'],
                author_id=serialized_data['author_id']
            ).create()
        except ValidationError as err:
            raise ValidationError(err.messages)
        except ObjectDoesNotExists:
            raise PoemAlreadyExists(serialized_data['name'])

    def get_poems(self):
        return PoemsShema(many=True).dump(Poems.read())
