from marshmallow import ValidationError

from app.poems.models import Authors
from app.poems.serializers import AuthorsSchema
from app.core.exceptions.models import ObjectDoesNotExists
from app.poems.exceptions.models import AuthorAlreadyExists, AuthorDoesNotExists


class AuthorManager:
    def create_new_author(self, data):
        try:
            serialized_data = AuthorsSchema().load(data)
            return Authors(
                first_name = serialized_data['first_name'],
                last_name = serialized_data['last_name']
            ).create()
        except ValidationError as err:
            raise ValidationError(err.messages)
        except ObjectDoesNotExists:
            raise AuthorAlreadyExists(
                serialized_data['first_name'] + ' ' + serialized_data['last_name']
            )

    def authors_list(self):
        authors = AuthorsSchema(many=True).dump(Authors.read())
        return authors

    def get_author(self, id):
        try:
            author = AuthorsSchema().dump(Authors.read(id=id)[0])
        except ObjectDoesNotExists:
            raise AuthorDoesNotExists()
        return author
