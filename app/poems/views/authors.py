from flask import request

from flask_restful import Resource
from flask_jwt_extended import jwt_required

from marshmallow import ValidationError

from app.app import api
from app.poems.domain import AuthorManager
from app.poems.serializers import AuthorsSchema
from app.poems.exceptions.models import AuthorAlreadyExists, AuthorDoesNotExists


class AuthorsView(Resource):
    @jwt_required()
    def post(self):
        try:
            data = request.get_json()
            manager = AuthorManager()
            author = manager.create_new_author(data)
            return AuthorsSchema().dump(author), 200
        except AuthorAlreadyExists as err:
            return {'error': err.messages}, 400
        except ValidationError as err:
            return {'error': err.messages}, 401


    @jwt_required()
    def get(self):
        manager = AuthorManager()
        return manager.authors_list(), 200


class AuthorView(Resource):
    @jwt_required()
    def get(self, author_id):
        try:
            manager = AuthorManager()
            author = manager.get_author(author_id)
            return author, 200
        except AuthorDoesNotExists as err:
            return {'error': err.messages}, 404




api.add_resource(AuthorsView, '/authors')
api.add_resource(AuthorView, '/authors/<author_id>')
