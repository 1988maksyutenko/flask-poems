from flask import request

from flask_restful import Resource
from flask_jwt_extended import jwt_required
from marshmallow import ValidationError

from app.app import api
from app.poems.domain import PoemsManager
from app.poems.serializers import PoemsShema
from app.poems.exceptions.models import PoemAlreadyExists


class PoemsView(Resource):

    @jwt_required()
    def post(self):
        data = request.get_json()
        manager = PoemsManager()
        try:
            poem = manager.create_new_poem(data)
            return PoemsShema().dump(poem), 200
        except ValidationError as err:
            return {'error': err.messages}, 401
        except PoemAlreadyExists as err:
            return {'error': err.messages}, 400

    @jwt_required()
    def get(self):
        manager = PoemsManager()
        poems = manager.get_poems()
        return poems, 200


api.add_resource(PoemsView, '/poems')
