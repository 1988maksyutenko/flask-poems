from marshmallow import Schema, fields


class PoemsShema(Schema):

    id = fields.Int()
    name = fields.Str()
    text = fields.Str()
    author_id = fields.Int()
