from marshmallow import Schema, fields


class AuthorsSchema(Schema):
    id = fields.Int()
    first_name = fields.Str()
    last_name = fields.Str()
